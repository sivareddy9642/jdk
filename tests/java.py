import testinfra.utils.ansible_runner
testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')

def test_directory_existance(File):
    assert File("/usr/local/jdk").is_directory

def test_maven_link(File):
    assert File('/usr/bin/java').is_symlink
